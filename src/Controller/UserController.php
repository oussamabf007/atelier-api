<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="new_user", methods={"POST"})
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $req = json_decode($request->getContent());

        $email = $req->email;
        $password = $req->password;

        if (!$email || !$password) {
            return $this->json([
                "code" => "400",
                "msg" => "verify your credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $user->setEmail($email);
        $user->setRoles(["ROLE_ADMIN"]);
        $user->setPassword($encoder->encodePassword($user, $password));

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($user);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            "msg" => "User Created Successfully",
            "email" => $user->getEmail(),
            "role" => $user->getRoles()
        ]);
    }
}
