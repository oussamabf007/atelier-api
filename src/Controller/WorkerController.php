<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Task;
use App\Entity\Worker;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class WorkerController extends AbstractController
{
    /**
     * @Route("/api/workers/init-worker", name="initialisation", methods={"GET"})
     */
    // public function init()
    // {
    //     $projects = $this->getDoctrine()->getRepository(Project::class)->findAllArray();
    //     $tasks = $this->getDoctrine()->getRepository(Task::class)->findAllArray();

    //     return $this->json([
    //         "code" => 200,
    //         "msg" => "init worker succeded",
    //         "data" => [
    //             "projects" => $projects,
    //             "tasks" => $tasks,
    //         ]
    //     ]);
    // }


    /**
     * @Route("/api/workers", name="list_workers", methods={"GET"})
     */
    public function index(): Response
    {
        $workers = $this->getDoctrine()->getRepository(Worker::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $workers
        ]);
    }

    /**
     * @Route("/api/workers/{id}", name="show_worker", methods={"GET"})
     */

    public function show($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $worker = $em->getRepository(Worker::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $worker[0]
        ]);
    }

    /**
     * @Route("/api/workers", name="create_worker", methods={"POST"})
     */
    public function create(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $cost = $req->cost;
        $post = $req->post;
        $phone = $req->phone;

        if (!$name || !$cost) {
            return $this->json([
                "code" => 400,
                "msg" => "Verify your credentials"
            ]);
        }
        $em = $this->getDoctrine()->getManager();


        $worker = new Worker();

        $worker->setName($name);
        $worker->setCost($cost);
        $worker->setPost($post);
        $worker->setPhone($phone);


        $worker->setCreatedAt(new DateTime());

        $em->persist($worker);
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Worker created successfully!",
            "name" => $worker->getName()
        ]);
    }

    /**
     * @Route("/api/workers/{id}", name="delete_worker", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $worker = $em->getRepository(Worker::class)->find($id);
        if (!$worker) {
            return $this->json([
                "msg" => "Worker does not exist!"
            ]);
        }
        $em->remove($worker);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Worker Deleted"
        ]);
    }

    /**
     * @Route("/api/workers/{id}", name="edit_worker", methods={"PUT"})
     */
    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $cost = $req->cost;
        $post = $req->post;
        $phone = $req->phone;

        if (!$name || !$cost) {
            return $this->json([
                "msg" => "name required"
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $worker = $em->getRepository(Worker::class)->find($id);
        if (!$worker) {
            return $this->json([
                "msg" => "Worker does not exist"
            ]);
        }
        $worker->setName($name);
        $worker->setCost($cost);
        $worker->setPost($post);
        $worker->setPhone($phone);


        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Worker Updated"
        ]);
    }
}
