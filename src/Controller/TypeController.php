<?php

namespace App\Controller;

use App\Entity\Type;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class TypeController extends AbstractController
{
    /**
     * @Route("/api/types", name="list_types", methods={"GET"})
     */
    public function index(): Response
    {
        $types = $this->getDoctrine()->getRepository(Type::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $types
        ]);
    }

    /**
     * @Route("/api/types/{id}", name="show_types", methods={"GET"})
     */

    public function show($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository(Type::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $type[0]
        ]);
    }

    /**
     * @Route("/api/types", name="create_type", methods={"POST"})
     */
    public function create(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $cost = $req->cost;

        if (!$name || !$cost) {
            return $this->json([
                "code" => 400,
                "msg" => "Verify your credentials"
            ]);
        }
        $em = $this->getDoctrine()->getManager();

        $type = new Type();
        $type->setName($name);
        $type->setCost($cost);

        $type->setCreatedAt(new DateTime());

        $em->persist($type);
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Type created successfully!",
            "name" => $type->getName()
        ]);
    }

    /**
     * @Route("/api/types/{id}", name="delete_type", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository(Type::class)->find($id);
        if (!$type) {
            return $this->json([
                "msg" => "Type does not exist!"
            ]);
        }
        $em->remove($type);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Type Deleted"
        ]);
    }

    /**
     * @Route("/api/types/{id}", name="edit_type", methods={"PUT"})
     */
    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $cost = $req->cost;

        if (!$name) {
            return $this->json([
                "msg" => "name required"
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository(Type::class)->find($id);
        if (!$type) {
            return $this->json([
                "msg" => "Type does not exist"
            ]);
        }
        $type->setName($name);
        $type->setCost($cost);

        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Type Updated"
        ]);
    }
}
