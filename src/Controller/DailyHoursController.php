<?php

namespace App\Controller;


use App\Entity\DailyHours;
use App\Entity\Task;
use App\Entity\Worker;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DailyHoursController extends AbstractController
{
    /**
     * @Route("/api/dailyhours", name="list_daily", methods={"GET"})
     */
    public function index(): Response
    {
        $dailys = $this->getDoctrine()->getRepository(DailyHours::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $dailys
        ]);
    }

    /**
     * @Route("/api/dailyhours/{id}", name="show_daily", methods={"GET"})
     */

    public function show($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $daily = $em->getRepository(DailyHours::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $daily[0]
        ]);
    }

    /**
     * @Route("/api/dailyhours", name="create_daily", methods={"POST"})
     */

    public function create(Request $request)
    {
        $req = json_decode($request->getContent());
        $taId = $req->task;
        $wId = $req->worker;

        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository(Task::class)->find($taId);
        $worker = $em->getRepository(Worker::class)->find($taId);

        $daily = new DailyHours();
    }
}
