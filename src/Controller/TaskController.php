<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\Type;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class TaskController extends AbstractController
{
    /**
     * @Route("/api/tasks", name="list_tasks", methods={"GET"})
     */
    public function index(): Response
    {
        $tasks = $this->getDoctrine()->getRepository(Task::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $tasks
        ]);
    }

    /**
     * @Route("/api/tasks/{id}", name="show_task", methods={"GET"})
     */

    public function show($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository(Task::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $task[0]
        ]);
    }

    /**
     * @Route("/api/tasks", name="create_task", methods={"POST"})
     */
    public function create(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $description = $req->description ?? "";
        $tyId = $req->type;
        if (!$name) {
            return $this->json([
                "code" => 400,
                "msg" => "Verify your credentials"
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository(Type::class)->find($tyId);

        $task = new Task();
        $task->setName($name);
        $task->setDescription($description);
        $task->setType($type);
        $task->setCreatedAt(new DateTime());

        $em->persist($task);
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Task created successfully!",
            "name" => $task->getName()
        ]);
    }

    /**
     * @Route("/api/tasks/{id}", name="delete_task", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository(Task::class)->find($id);
        if (!$task) {
            return $this->json([
                "msg" => "Task does not exist!"
            ]);
        }
        $em->remove($task);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Task Deleted"
        ]);
    }

    /**
     * @Route("/api/tasks/{id}", name="edit_task", methods={"PUT"})
     */
    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $description = $req->description ?? "";
        $tyId = $req->type;
        if (!$name) {
            return $this->json([
                "msg" => "name required"
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository(Task::class)->find($id);
        $type = $em->getRepository(Type::class)->find($tyId);
        if (!$task) {
            return $this->json([
                "msg" => "Task does not exist"
            ]);
        }
        $task->setName($name);
        $task->setDescription($description);
        $task->settype($type);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Task Updated"
        ]);
    }
}
