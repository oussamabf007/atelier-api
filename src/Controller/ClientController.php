<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Client;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ClientController extends AbstractController
{


    /**
     * @Route("/api/clients", name="list_clients", methods={"GET"})
     */
    public function index(): Response
    {
        $clients = $this->getDoctrine()->getRepository(Client::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $clients
        ]);
    }

    /**
     * @Route("/api/clients/{id}", name="show_clients", methods={"GET"})
     */

    public function show($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository(Client::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $client[0]
        ]);
    }

    /**
     * @Route("/api/clients", name="create_clients", methods={"POST"})
     */
    public function create(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $email = $req->email;
        $phone = $req->phone;


        if (!$name) {
            return $this->json([
                "code" => 400,
                "msg" => "Verify your credentials"
            ]);
        }
        $em = $this->getDoctrine()->getManager();



        $client = new Client();

        $client->setName($name);
        $client->setEmail($email);
        $client->setPhone($phone);


        $client->setCreatedAt(new DateTime());

        $em->persist($client);
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Client created successfully!",
            "name" => $client->getName()
        ]);
    }

    /**
     * @Route("/api/clients/{id}", name="delete_clients", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository(Client::class)->find($id);
        if (!$client) {
            return $this->json([
                "msg" => "Client does not exist!"
            ]);
        }
        $em->remove($client);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Client Deleted"
        ]);
    }

    /**
     * @Route("/api/clients/{id}", name="edit_clients", methods={"PUT"})
     */
    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $email = $req->email;
        $phone = $req->phone;
        if (!$name) {
            return $this->json([
                "msg" => "name required"
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository(Client::class)->find($id);
        if (!$client) {
            return $this->json([
                "msg" => "Client does not exist"
            ]);
        }
        $client->setName($name);
        $client->setEmail($email);
        $client->setPhone($phone);


        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Client Updated"
        ]);
    }
}
