<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Client;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ProjectController extends AbstractController
{

    /**
     * @Route("/api/projects", name="list_projects", methods={"GET"})
     */
    public function index(): Response
    {
        $projects = $this->getDoctrine()->getRepository(Project::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $projects
        ]);
    }

    /**
     * @Route("/api/projects/{id}", name="show_project", methods={"GET"})
     */

    public function show($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $project[0]
        ]);
    }

    /**
     * @Route("/api/projects", name="create_project", methods={"POST"})
     */
    public function create(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $description = $req->description ?? "";
        $cId = $req->client;
        if (!$name) {
            return $this->json([
                "code" => 400,
                "msg" => "Verify your credentials"
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository(Client::class)->find($cId);

        $project = new Project();
        $project->setName($name);
        $project->setDescription($description);
        $project->setClient($client);
        $project->setCreatedAt(new DateTime());

        $em->persist($project);
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Project created successfully!",
            "name" => $project->getName()
        ]);
    }

    /**
     * @Route("/api/projects/{id}", name="delete_project", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->find($id);
        if (!$project) {
            return $this->json([
                "msg" => "Project does not exist!"
            ]);
        }
        $em->remove($project);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Project Deleted"
        ]);
    }

    /**
     * @Route("/api/projects/{id}", name="edit_project", methods={"PUT"})
     */
    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $description = $req->description ?? "";
        $cId = $req->client;
        if (!$name) {
            return $this->json([
                "msg" => "name required"
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository(Project::class)->find($id);
        $client = $em->getRepository(Client::class)->find($cId);
        if (!$project) {
            return $this->json([
                "msg" => "Project does not exist"
            ]);
        }
        $project->setName($name);
        $project->setDescription($description);
        $project->setClient($client);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Project Updated"
        ]);
    }
}
