<?php

namespace App\Repository;

use App\Entity\DailyHours;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DailyHours|null find($id, $lockMode = null, $lockVersion = null)
 * @method DailyHours|null findOneBy(array $criteria, array $orderBy = null)
 * @method DailyHours[]    findAll()
 * @method DailyHours[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DailyHoursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DailyHours::class);
    }

    // /**
    //  * @return DailyHours[] Returns an array of DailyHours objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DailyHours
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
